# Fonctions

print("Hello")

`print` est une fonction, `("Hello")` est un argument

la fonction `type` permet de connaître le type de données

# Variables

- code alphanumérique lié à une donnée
- codee dans lequle on va ranger une donnée

```
>>> a = "hello world"
>>> print(a)
hello world
```

## Réaffectation des variables 

```
a = 1
a = a + 1
a = a + 2
```

Changer la variable au cours du temps est une pratique courante. Autre application possible : **les itérateus**

