
# Exercice

Créer une fonction pour la table de multiplication de 7.


```python
def table_7():
    for i in range(1, 21):
        print(7 * i)
table_7()
```

    7
    14
    21
    28
    35
    42
    49
    56
    63
    70
    77
    84
    91
    98
    105
    112
    119
    126
    133
    140


# Plusieurs exercices

- Maintenant, on modifie la fonction de table de 7 pour avoir en paramètre la base de la table, le premier terme et l'indice du dernier terme.

## Conversion

- Écrire une fonction avec 1 paramètre pour convertir les degrés celsius à fahrenheit.
- Généraliser cette fonction avec 3 paramètres : les deux noms d'unités et le taux

## Conversion seconde

- Écrire une fonction pour convertir le nombre de secondes en années mois et jours, heures, minutes.

## Volume

- Écrire une fonction pour calculer le volume d'un parallèlépipède. 

## Bonus

- Écrire une fonction qui convertit une vitesse donnée par l'utilisateur en miles ou en km (1 mile = 1609 mètres) en utilisant la fonction conversion.
- Déterminer si une année (dont le millésime est introduit pra l'utilisateur) est bissextile ou non. Une année A est bissextile si A est divisible par 4. Elle ne l'est cependant pas si A est un multiple de 100, à moins que A ne soit multiple de 400.


```python
def table_mult(chiffre, début, fin):
    '''
    Premier exercice : fonction de table de multiplication
    '''
    table = []
    for i in range(début, fin):
        table.append(chiffre * i)
    return table
table_mult(5, 6, 13)
```




    [30, 35, 40, 45, 50, 55, 60]




```python
def celsius_to_fahrenheit(deg_c):
    '''
    Conversion de celsius à fahrenheit
    '''
    deg_f = deg_c * 9 / 5 + 32
    return deg_f
celsius_to_fahrenheit(26)
```




    78.8




```python
def convert(unit1, unit2, value, rate, addition):
    '''
    Fonction générique de conversion de unit1 vers unit2
    '''
    result = value * rate + addition
    print(f"{value} {unit1} = {result} {unit2}")
    
convert("°C", "°F", 26, 9 / 5, 32)
```

    26 °C = 78.80000000000001 °F



```python
def convert_seconds(seconds):
    '''
    Convertir un grand nombre de secondes en années, mois et jours, heures, minutes
    '''
    minutes = seconds / 60
    hours = minutes / 60
    days = hours / 24
    months = days / 30
    years = days / 365
    return minutes, hours, days, months, years

convert_seconds(123456789)
```




    (2057613.15,
     34293.5525,
     1428.8980208333332,
     47.62993402777777,
     3.914789098173516)




```python
def volume_par(height, width, length):
    '''
    Calculer le volume d'un parallélépipède
    '''
    return height * width * length
volume_par(2, 5, 8)
```




    80




```python
def determine_leapyear(year):
    '''
    Déterminer si une année (dont le millésime est introduit pra l'utilisateur) est bissextile ou non
    '''
    if year % 100 == 0:
        if year % 400 == 0:
            return True
        else: 
            return False
    elif year % 4 == 0:
        return True
    else:
        return False
determine_leapyear(1900)
```




    False




```python
def miles_to_km(miles):
    convert("miles", "kilometers", )
```

# Listes

- Une **liste** est un objet dans lequel on peut stocker plusieurs ariables : donées composites.
- Séquence ordonnée d'éléments accessibles par un **index**. L'index commence toujours par zéro.
- **Attention :** les méthodes des listes modifient l'objet et ne renvoient rien contrairement aux chaînes de caractères.

## Créer une liste

- Une liste vide : `nom_liste = []`
- Une liste avec des valeurs : `nom_liste = [valeur1, valeur2, valeur3]`
- Possibilité d'avoir des string, des valeurs numériques et des listes dans une même liste

## Afficher les éléments d'une liste

- Afficher le contenu d'une liste : `nom_liste`
- Afficher un élément d'une liste grâce à son index (identique à la chaîne de caractères) :
    - `ma_liste = ["Jean", "Paul", "Pierre"]`
    - `ma_liste[2]` --> `"Paul"`
    
## Modifier ou ajouter un élément

- Modifier un élément :
    ```python
    ma_liste = ["Jean", "Paul", "Pierre"]
    ma_liste[1] = "Maria"
    ```
- Ajouter un élément à la fin d'une liste : `ma_liste.append("Louise")`. On peut ajouter plusieurs à la fois, séparés par des virgules.
- Ajouter un élément à l'endroit voulu dans une liste : `ma_liste.insert(1, "Jeanne")`




```python
ma_phrase1 = "Bonjour"
ma_phrase2 = ma_phrase1.upper()
print(ma_phrase1, ma_phrase2)
```

    Bonjour BONJOUR


Appliquer une méthode à une liste modifie **toujours** la liste :


```python
liste1 = [1, 3, "chien"]
liste2 = liste1.append("chat")
print(liste1)
print(liste2)
```

    [1, 3, 'chien', 'chat']
    None


La méthode liste.copy() crée une copie.

## Concaténation de deux listes

- 3 méthodes : extend, + ou +=


```python
ma_liste1 = [1, 2, 3]
ma_liste2 = [4, 5, 6]
ma_liste1.extend(ma_liste2)
print(ma_liste1)
```

    [1, 2, 3, 4, 5, 6]



```python
ma_liste1 = [1, 2, 3]
ma_liste2 = [4, 5, 6]
print(ma_liste1 + ma_liste2)
```

    [1, 2, 3, 4, 5, 6]



```python
ma_liste1 = [1, 2, 3]
ma_liste2 = [4, 5, 6]
ma_liste1 += ma_liste2
print(ma_liste1)
```

    [1, 2, 3, 4, 5, 6]


## Supression d'un élément

- méthode `del` : supprimer une variable ou un élément d'une liste avec son index.


```python
ma_liste = [1, 2, 3]
del ma_liste[1]
print(ma_liste)
```

    [1, 3]


- méthode `remove` : supprimer directement l'élément d'une liste


```python
ma_liste = [1, 2, 3]
ma_liste.remove(1)
print(ma_liste)
```

    [2, 3]


## Opération sur les listes

- comme pour les chaînes de caractères :
    - `len(ma_liste)` : taille d'une liste
    - `ma_liste.count("mon_élément")` : trouver le nombre d'occurences d'un élément dans une liste
    - `ma_liste.index("mon_élément")` : renvoie l'index d'un élément
