<div class="cell markdown">

# Cours python du 28 février 2019

## Boucles (continuation)

### Fonction `range`

La fonction `range` prend trois arguments : début, fin, pas. Par exemple
:

</div>

<div class="cell code" data-execution_count="4">

``` python
for i in range(1, 10, 2):
    print(i)
```

<div class="output stream stdout">

    1
    3
    5
    7
    9

</div>

</div>

<div class="cell markdown">

## Exercice

  - Afficher la liste des carrés et des cubes des nombres de 1 à 12
  - Ajouter du texte avant chaque nombre ("ceci est le carré/ceci est le
    cube")
  - Afficher cette liste sans retour à la ligne (utiliser l'argument
    `end = " "` à la fonction print)

</div>

<div class="cell code" data-execution_count="5">

``` python
for i in range(1, 13):
    print(f"{i ** 2} est le carré de {i}. {i ** 3} est le cube de {i}.", end = " ")
```

<div class="output stream stdout">

    1 est le carré de 1. 1 est le cube de 1. 4 est le carré de 2. 8 est le cube de 2. 9 est le carré de 3. 27 est le cube de 3. 16 est le carré de 4. 64 est le cube de 4. 25 est le carré de 5. 125 est le cube de 5. 36 est le carré de 6. 216 est le cube de 6. 49 est le carré de 7. 343 est le cube de 7. 64 est le carré de 8. 512 est le cube de 8. 81 est le carré de 9. 729 est le cube de 9. 100 est le carré de 10. 1000 est le cube de 10. 121 est le carré de 11. 1331 est le cube de 11. 144 est le carré de 12. 1728 est le cube de 12. 

</div>

</div>

<div class="cell markdown">

## Exercice

  - Écrire un programme qui affiche un table de conversion exprimée en
    euros et en francs suisses. La progression sera géométrique (1€, 2€,
    4€, 8€ etc.)
  - 1€ = 1,15 CHF
  - S'arrêter à 16384€

</div>

<div class="cell code" data-execution_count="40">

``` python
euros = 1
while euros <= 16384:
    chf = euros * 1.15
    if euros < 2:
        print(f"{euros} euro = {chf:.2f} franc suisse.")
    else:
        print(f"{euros} euros = {chf:.2f} francs suisses.")
    euros *= 2
```

<div class="output stream stdout">

    1 euro = 1.15 franc suisse.
    2 euros = 2.30 francs suisses.
    4 euros = 4.60 francs suisses.
    8 euros = 9.20 francs suisses.
    16 euros = 18.40 francs suisses.
    32 euros = 36.80 francs suisses.
    64 euros = 73.60 francs suisses.
    128 euros = 147.20 francs suisses.
    256 euros = 294.40 francs suisses.
    512 euros = 588.80 francs suisses.
    1024 euros = 1177.60 francs suisses.
    2048 euros = 2355.20 francs suisses.
    4096 euros = 4710.40 francs suisses.
    8192 euros = 9420.80 francs suisses.
    16384 euros = 18841.60 francs suisses.

</div>

</div>

<div class="cell markdown">

## Boucles dans des boucles

Que fait le programme suivant ?

``` python
number = 1
while number <= 10:
    print(number)
    number += 1
print('Done!')
```

Il affiche les nombres de 1 à 9, puis "Done\!"

Et celui-ci ?

``` python
number = 1
while number < 1024:
    print(number)
    number *= 2
```

Afficher les nombres, chaque fois en multipliant par 2.

Et celui-ci ?

``` python
number = 1
while number > 1024:
    print(number)
    number *= 2
```

Il ne va rien faire : comme `number` est égal à 1, il ne va jamais
rentrer dans la boucle qui commence seulement lorsque `number` est
supérieur à 1024. La seule chose qui arrive est que 1 est affecté au
variable.

Et celui-ci ?

``` python
number = 1025
while number > 1024:
    print(number)
    number *= 2
```

Boucle infinie. On va continuer à multiplier 1025 par 2 à l'infini.

Et le suivant ?

``` python
tolerance = 1.0e-15
uncertainty = 2.0
while uncertainty > tolerance:
    uncertainty /= 2.0
    print(uncertainty)
print('Final uncertainty:', uncertainty)
```

On va prendre 2.0, diviser le résultat par 2 chauque fois jusqu'à ce
qu'on arrive à 1.0e-15.

``` python
text = 'OK'
while text != '':
    text = input('Give me some text')
    print(text)
```

</div>

<div class="cell code" data-execution_count="43">

``` python
text = 'OK'
while text != '':
    text = input('Give me some text!')
    print(text)
```

<div class="output stream stdin">

    Give me some text! Yeah

</div>

<div class="output stream stdout">

    Yeah

</div>

<div class="output stream stdin">

    Give me some text! something

</div>

<div class="output stream stdout">

    something

</div>

<div class="output stream stdin">

    Give me some text! blah

</div>

<div class="output stream stdout">

    blah

</div>

<div class="output stream stdin">

    Give me some text! yadda yadda yadda

</div>

<div class="output stream stdout">

    yadda yadda yadda

</div>

<div class="output stream stdin">

``` 
Give me some text! 
```

</div>

<div class="output stream stdout">

``` 

```

</div>

</div>

<div class="cell markdown">

## Un autre exercice

Vous avez 400€ pour les vacances.

Option 1 :

  - 4 nuits à 60€/nuit.
  - Transport à 80€.
  - Repas : 10€/jour (1.1) ou à 20€/jour (1.2)

Option 2 :

  - 5 nuits à 40€/nuit
  - transport à 100€
  - repas 6€/jour ou 20€/jour

Option 3 :

  - 2 nuits à 100€/nuit
  - transport à 200€
  - repas à 7€/jour ou 30€/jour

Produire un texte du type : "Avec l'option X.Y, jeux peux partir et il
me restera ...€" ou "...je ne peux pas partir car il me manque ...€".

</div>

<div class="cell code" data-execution_count="60">

``` python
cash = 400
option = input("Choisir l'option:")
if option == "1.1":
    cash = cash - 60 * 4 - 80 - 10 * 4
elif option == "1.2":
    cash = cash - 60 * 4 - 80 - 20 * 4
elif option == "2.1":
    cash = cash - 40 * 5 - 100 - 6 * 5
elif option == "2.2":
    cash = cash - 40 * 5 - 100 - 20 * 5
elif option == "3.1":
    cash = cash - 100 * 2 - 200 - 7 * 2
elif option == "3.2":
    cash = cash - 100 * 2 - 200 - 30 * 2
else:
    print("Option invalide.")
if cash > 0 :
    print(f"Avec l'option {option}, je peux partir et il me restera {cash} €.")
elif cash == 0 :
    print(f"Avec l'option {option}, je peux partir mais il ne me plus rien de côté.")
else:
    print(f"Avec l'option {option}, je ne peux pas partir car il me manque {cash * -1} €.")
```

<div class="output stream stdin">

    Choisir l'option: 2.2

</div>

<div class="output stream stdout">

    Avec l'option 2.2, je peux partir mais il ne me plus rien de côté.

</div>

</div>

<div class="cell code" data-execution_count="6">

``` python
def prix(nuits, hotel, transport, repas):
    cash = 400 - nuits * hotel - transport - nuits * repas
    if cash > 0 :
        print(f"Avec l'option {option_a}.{option_b}, je peux partir et il me restera {cash} €.")
    elif cash == 0 :
        print(f"Avec l'option {option_a}.{option_b}, je peux partir mais il ne me restera plus rien de côté.")
    else:
        print(f"Avec l'option {option_a}.{option_b}, je ne peux pas partir car il me manque {cash * -1} €.")


option_a = 1
option_b = 1
prix(4, 60, 80, 10)
option_b = 2
prix(4, 60, 80, 20)
option_a = 2
option_b = 1
prix(5, 40, 100, 6)
option_b = 2
prix(5, 40, 100, 20)
option_a = 3
option_b = 1
prix(2, 100, 200, 7)
option_b = 2
prix(2, 100, 200, 30)
```

<div class="output stream stdout">

    Avec l'option 1.1, je peux partir et il me restera 40 €.
    Avec l'option 1.2, je peux partir mais il ne me restera plus rien de côté.
    Avec l'option 2.1, je peux partir et il me restera 70 €.
    Avec l'option 2.2, je peux partir mais il ne me restera plus rien de côté.
    Avec l'option 3.1, je ne peux pas partir car il me manque 14 €.
    Avec l'option 3.2, je ne peux pas partir car il me manque 60 €.

</div>

</div>

<div class="cell markdown">

# Fonctions

Permettent de décomposer un programme en sous-programme réutilisable.

  - Fonctions prédéfinies : déjà définies dans le langage
  - Fonctions originales : définies par l'utilisateur

En python, les fonctions s'écrivent toujours en minuscules et sont
suivis de parenthèses : par exemple, `range()`

## Modules

Fichiers qui regroupent des ensembles de fonctions (égalemeent des
définitions de classes et de variables)

Il existe un grand nombre de modules pré-programmés présents avec
python.

Un ensemble de fonctions apparentées est une **bibliothèque**.

<https://docs.python.org/3/py-modindex.html>

### Un exemple de module

  - Le module "math" contient des fonctions prédéfinies. Par exemple,
    pour calculer la racine carré d'un nombre : `sqrt()`. Calculer le
    sinus d'une angle en radians : `sin()`
  - Appel d'un module : `from nom_module import nom_fonction`

### Exercice

Écrire un script qui calcule la racine carrée de 144 et le sinus d'un
angle de pi/3.

</div>

<div class="cell code" data-execution_count="66">

``` python
import math
print(f"Racine carrée de 144 : {math.sqrt(144)}")
print(f"Sinus de pi/3 : {math.sin(math.pi/3)}")
```

<div class="output stream stdout">

    Racine carrée de 144 : 12.0
    Sinus de pi/3 : 0.8660254037844386

</div>

</div>

<div class="cell markdown">

### Exercice

Écrivez un programme qui calcule le périmètre et l'aire d'un triangle
quelconque dont l'utilisateur fournit la longueur des trois côtés.

  - Formule périmètre : a + b + c
  - Formule aire : sqrt((d \* (d - a) \* (d - b) \* (d - c))) où d est
    le demi périmètre

</div>

<div class="cell code" data-execution_count="2">

``` python
from math import sqrt
a = 1
b = 8
c = 8
perimetre = a + b + c
d = perimetre / 2
aire = sqrt(d * (d - a) * (d - b) * (d - c))
print(f"Périmètre : {perimetre}")
print(f"Aire : {aire:.3f}")
```

<div class="output stream stdout">

    Périmètre : 17
    Aire : 3.992

</div>

</div>

<div class="cell markdown">

## Fonctions avec ou sans argument

  - Par ex., `print()` est valable (mais ne fait rien), alors que
    `math.sqrt()` a besoin d'un argument, sinon il y a échec :

<!-- end list -->

    >>> math.sqrt()
    
    ---------------------------------------------------------------------------
    TypeError                                 Traceback (most recent call last)
    <ipython-input-82-84842f756272> in <module>
    ----> 1 math.sqrt()
    
    TypeError: sqrt() takes exactly one argument (0 given)

</div>

<div class="cell markdown">

D'autres fonctions prennent plusieurs argument, comme par ex. `range()`.

## Définir une fonction : `abs_mult`

  - créer une table de multiplication en valeur absolue
  - Ensuite, créer une fonction qui ne prend pas de paramètre et qui
    retourne les 10 premier éléments de la table de 7. Puis appeler la
    fonction.

Pour définir :

``` python
def abs_mult(par1, par2, etc):
```

`def` est suivi du nom de la fonction et des parenthèses (avec ou sans
paramètres), puis un `:`. Les lignes suivantes sont indentées (comme les
boucles).

On évite généralement de mettre des `print` dans les fonctions, à une
exception près : c'est très utile quand on est en train de tester.

</div>

<div class="cell code" data-execution_count="88">

``` python
def abs_mult(n1, n2):
    if n1 * n2 >= 0:
        result = n1 * n2
    else:
        result = -(n1 * n2)
    return result

abs_mult(7, -7)
```

<div class="output execute_result" data-execution_count="88">

``` 
49
```

</div>

</div>

<div class="cell code" data-execution_count="103">

``` python
# Une fonction qui n'affiche que du texte. Par défaut, il n'affiche le texte qu'une fois.

def loremipsum(n = None):
    if n == None:
        n = 1
    print("Lorem ipsum dolor sit amet. " * n)
loremipsum()
loremipsum(7)
```

<div class="output stream stdout">

``` 
Lorem ipsum dolor sit amet. 
Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet. 
```

</div>

</div>

<div class="cell markdown">

# Exercice

Conversion entre °F et °C avec une fonction.

</div>

<div class="cell code" data-execution_count="3">

``` python
def f_to_c(deg_f):
    deg_c = (deg_f - 32) * 5 / 9
    return deg_c

def c_to_f(deg_c):
    deg_f = deg_c * 9 / 5 + 32
    return deg_f

unit = input("(1) Convertir fahrenheit vers celsius\n(2) Convertir celsius vers fahrenheit\n")
if unit == "1":
    temp = float(input("Combien de degrés fahrenheit ?"))
    print(f"{temp}°F = {f_to_c(temp):.1f}°C")
elif unit == "2":
    temp = float(input("Combien de degrés celsius ?"))
    print(f"{temp}°C = {c_to_f(temp):.1f}°F")
else:
    print("Je n'ai pas compris.")
```

<div class="output stream stdin">

    (1) Convertir fahrenheit vers celsius
    (2) Convertir celsius vers fahrenheit
     2
    Combien de degrés celsius ? 31

</div>

<div class="output stream stdout">

    31.0°C = 87.8°F

</div>

</div>
