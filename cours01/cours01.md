# Pourquoi python ?

- Code facile à apprendre et à écrire
- Riche en librairies, grande communauté, open source
- Langage interprété : pas nécessaire de compiler avant exécution du code

# Installation

- anaconda avec python 3.7
- télécharger un IDE : atom ou pycharm

# Quelques caractéristiques

**Erreurs** : C'est une bonne idée d'exécuter le code souvent, petit à petit, pour apprendre de ses erreurs.

**Fonctions** : identifiables par les parenthèses

# Premier script

# Type de données : string

- `type("hello")`
- string : chaîne de caractères ordonnés
- définis par ', ", ou '''

# Retour à la ligne, tabulation

- retour à la ligne : `\n`
- tabulation : `\t`
- texte sur plusieurs lignes : '''texte'''

# Variables

- Code alphanumérique lié à une donnée
- affecter une valeur à une variable : `nom_de_la_variable = valeur`
- Possibilité d'affecter une nouvelle valeur à une variable
- Possibilité de faire des calculs et d'enregistrer une nouvelle variable
- permutation : 
  ```
  a = "Jean"
  b = "Pierre"
  a,b = b,a
  ```
- Plusieurs variables pour une même valeur : `a = b = 2`
- avoir une instruction sur plusieurs lignes a = b = 2 / 3 + 4 + 2

# PEP 8 : convention pour le nommage

- dans tous les cas : ne pas commencer par un chiffre
- noms des variables et des fonctions
  - lettres minuscules : `speed`
  - utiliser un underscore pour séparer les mots : `speedy_gonzalez`
- nom des constantes :
  - en majuscules : `MAXIMUM`
  - underscores pour séparer les mots : `MAX_HEADROOM`
- noms des classes :
  - commencer par un majuscule :

# Mots réservés

```
import keyword
print(keyword.kwlist)
```

# Types de données

- strings
- nombres : int et float
- boolean : deux valeurs uniquement
- convertir des données :
  - `s = str(3.14159)`
  - `i = int("-42")`
  - `f = float(42)`

# Opérateurs

+ - * / // ** %