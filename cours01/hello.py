#!/usr/bin/env python3

a = "Hello world!"
print(a)
a = "Hello"
print(a)
a = a + " world."
print(a)