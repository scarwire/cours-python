#!/usr/bin/env python

# exercice 2

print("Exercice 2\n==========\n")

mes_phrases = "A handful of lawsuits in other arenas have challenged this practice. After Idaho’s Medicaid program started using an automated system to calculate benefits, recipients suddenly saw their benefits cut by as much as 30 percent."

def remove_numbers(string: str):
    """
    Quesiton 2.1. Ne pas supprimer les chiffres dans la variable mais le faire à l’aide d’une fonction. Cette fonction doit pouvoir supprimer n’importe quel chiffre de 0 à 9 (nombre entier).
    """
    numbers = "0123456789"
    for n in numbers:
        string = string.replace(n, "")
    string = string.replace("  ", " ")
    return string

mes_phrases_sans_chiffre = remove_numbers(mes_phrases)
print("2.1. " + mes_phrases_sans_chiffre + "\n")

def lowercase(string: str):
    """
    Question 2.2. Créer une fonction qui prend en argument une phrase qui contient des majuscules et qui retourne une phrase uniquement en minuscule.
    """
    return string.lower()

mes_phrases_minuscules = lowercase(mes_phrases)
print("2.2. " + mes_phrases_minuscules + "\n")

def lowquality_crypt(string: str):
    '''
    Quesiton 2.3. Remplacer chaque lettre de « mes_phrases_en_minuscule » par la lettre qui se situe 2 rang plus loin dans l’alphabet.
    '''
    #alphabet = "abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqurstuvwxyz"
    alphabet = "abcdefghijklmnopqrstuvwxyz"
    newstring = str()
    for char in string:
        if char in alphabet:
            i = alphabet.index(char)
            if i + 2 <= len(alphabet) - 1:
                newstring += alphabet[i + 2]
            else:
                newstring += alphabet[i + 2 - len(alphabet)]
        else:
            newstring += char
    return newstring

mes_phrases_chiffrees = lowquality_crypt(mes_phrases_minuscules)
print("2.3. " + mes_phrases_chiffrees + "\n")

def notmuchbetter_crypt(string: str, shift: int):
    '''
    Question 2.4. Réécrire la fonction précédente qui prend en argument une phrase et le rang des lettres à modifier dans l’alphabet.
    '''
    alphabet = "abcdefghijklmnopqrstuvwxyz"
    newstring = str()
    for char in string:
        if char in alphabet:
            i = alphabet.index(char)
            newstring += alphabet[(i + shift) % len(alphabet)]
        else:
            newstring += char
    return newstring

mes_phrases_chiffrees = notmuchbetter_crypt(mes_phrases_minuscules, 17)
print("2.3. Exemple de modification par 17 lettres : " + mes_phrases_chiffrees + "\n")

def stillnotgreat_crypt(string: str, shift: int):
    """
    Réécrire la fonction en 2.4 qui pourra prendre en argument une phrase avec des majuscules et des minuscules et l’écart des lettres choisis de l’alphabet.
    """
    alphabet = "abcdefghijklmnopqrstuvwxyz"
    newstring = str()
    for char in string:
        if char in alphabet:
            i = alphabet.index(char)
            newstring += alphabet[(i + shift) % len(alphabet)]
        elif char in alphabet.upper():
            i = alphabet.upper().index(char)
            newstring += alphabet.upper()[(i + shift) % len(alphabet)]
        else:
            newstring += char
    return newstring

print("2.4. Exemple de modification par 8 lettres: " + stillnotgreat_crypt(mes_phrases_sans_chiffre, 8) + "\n")

def neverthelessundependable_crypt(string:str):
    """
    Adapter la fonction en 2.5 pour prendre en argument une phrase avec des majuscule, minuscule et des nombres (la variable « mes_phrases »). Ajoutez 2 à chaque nombre et changer les lettres par les lettres situés deux rangs en arrière dans l’alphabet.
    """
    alphabet = "zyxwvutsrqponmlkjihgfedcbazyxwvutsrqponmlkjihgfedcba"
    numbers = "01234567890123456789"
    newstring = str()
    for char in string:
        if char in alphabet:
            i = alphabet.index(char)
            newstring += alphabet[i + 2]
        elif char in alphabet.upper():
            i = alphabet.upper().index(char)
            newstring += alphabet.upper()[i + 2]
        elif char in numbers:
            i = numbers.index(char)
            newstring += numbers[i + 2]
        else:
            newstring += char
    return newstring

print("2.5. " + neverthelessundependable_crypt(mes_phrases) + "\n")
