#!/usr/bin/env python

# exercice 5

from examen_ex3 import liste_traitee, occurrence_tr
interactive = True

print("Exercice 5\n==========\n")

with open('text_1.txt') as open_1:
    article_1 = open_1.read()
open_1.closed
with open('text_2.txt') as open_2:
    article_2 = open_2.read()
open_2.closed

print("5.1. Texte 1 :\n\n" + article_1 + "\n\nTexte 2 :\n\n" + article_2 + "\n")

def remove_junk(text: str):
    text = text.replace("\n", " ")
    text = text.replace("  ", " ")
    return text

def trouver_plus_mots(text1: str, text2: str):
    '''
    5.2. Fonction pour déterminer quel texte contient le plus de mot. Retourne 0 si le premier texte est plus long, 1 si le deuxième est plus long, -1 si les textes sont égaux.
    '''
    list1 = remove_junk(text1).split()
    list2 = remove_junk(text2).split()
    if len(list1) > len(list2):
        print("5.2. C'est le premier texte qui contient le plus de mots.\n")
        return 0
    elif len(list1) < len(list2):
        print("5.2. C'est le deuxième texte qui contient le plus de mots.\n")
        return 1
    elif len(list1) == len (list2):
        print("5.2. Les deux textes contiennent exactement le même nombre de mots.\n")
        return -1

trouver_plus_mots(article_1, article_2)

def trouver_plus_phrases(text1: str, text2: str):
    '''
    5.3. Fonction pour déterminer quel texte contient le plus de phrases. Retourne 0 si le premier texte est plus long, 1 si le deuxième est plus long, -1 si les textes sont égaux.
    '''
    list1 = remove_junk(text1).split(". ")
    list2 = remove_junk(text2).split(". ")
    if len(list1) > len(list2):
        print("5.3. C'est le premier texte qui contient le plus de phrases.\n")
        return 0
    elif len(list1) < len(list2):
        print("5.3. C'est le deuxième texte qui contient le plus de phrases.\n")
        return 1
    elif len(list1) == len (list2):
        print("5.2. Les deux textes contiennent exactement le même nombre de phrases.\n")
        return -1

trouver_plus_phrases(article_1, article_2)

def trouver_oc_mot(word:str, text1: str, text2: str):
    '''
    5.4. Créer une fonction pour savoir quel texte contient le plus de fois un mot de votre choix (qui devra être un argument de votre fonction). Retourne 0 si c'est le premier texte, 1 si c'est le deuxième, -1 s'il y a le même nombre d'occurrences.
    '''
    if word in occurrence_tr(liste_traitee(text1)):
        result1 = occurrence_tr(liste_traitee(text1))[word]
    else:
        result1 = 0
    if word in occurrence_tr(liste_traitee(text2)):
        result2 = occurrence_tr(liste_traitee(text2))[word]
    else:
        result2 = 0
    if result1 > result2:
        print(f"""C'est le premier texte qui contient le plus d'occurrences du mot “{word}” ({result1}).""")
        return 0
    elif result1 < result2:
        print(f"""C'est le deuxième texte qui contient le plus d'occurrences du mot “{word}” ({result2}).""")
        return 1
    elif result1 == result2:
        print(f"""Les deux textes contiennent le même nombre d'occurrences du mot “{word}” ({result2}).""")
        return -1

if interactive == True:
    searchword = input("5.4. Mot à rechercher dans le texte : ")
else:
    searchword = "is"
    print("5.4. Comparaison du nombre d'occurrences du mot “is” dans les deux textes.\n")
trouver_oc_mot(searchword, article_1, article_2)
