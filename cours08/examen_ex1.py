#!/usr/bin/env python

# exercice 1

print("Exercice 1\n==========\n")

mes_phrases = "A handful of lawsuits in other arenas have challenged this practice. After Idaho’s Medicaid program started using an automated system to calculate benefits, recipients suddenly saw their benefits cut by as much as 30 percent."

print("1.1. " + mes_phrases + "\n")

def remove_vowels(string: str):
    '''
    Question 1.2. Supprimer toutes les voyelles d'un texte.
    '''
    vowels = "AEIOUaeiou"
    for vowel in vowels:
        string = string.replace(vowel, "")
    return string

print("1.2. " + remove_vowels(mes_phrases) + "\n")

def remove_spaces(string: str):
    '''
    Question 1.3. Faire une chaine de caractère sans espace avec tous les mots de votre phrase.
    '''
    return string.replace(" ", "")

print("1.3. " + remove_spaces(mes_phrases) + "\n")

def reverse(string: str):
    '''
    Question 1.4. Inverser l’ordre des mots du texte. Par exemple : « Voici une phrase à inverser. » donnera « .inverser à phrase une Voici »
    '''
    i = -1
    punctuation = ",.’!?"
    nopunctstring = str()
    for char in string:
        if char not in punctuation:
            nopunctstring += char
    newstring = str()
    while i >= -(len(nopunctstring.split())):
        newstring += nopunctstring.split()[i] + " "
        i -= 1
    return newstring

print("1.4. " + reverse(mes_phrases) + "\n")

def reverse_letters(string: str):
    '''
    Question 1.4b. Inverser l’ordre des lettres du texte.
    '''
    i = -1
    newstring = str()
    while i >= -(len(string)):
        newstring += string[i]
        i -= 1
    return newstring

print("1.4b. " + reverse_letters(mes_phrases) + "\n")

def initial_caps(string: str):
    """
    Question 1.5. Mettre une majuscule à chaque première lettre de chaque mot. Par exemple : « Voici une phrase » donnera : « Voici Une Phrase.».
    """
    newstring = str()
    for word in string.split():
        newstring += word.capitalize() + " "
    return newstring

print("1.5. " + initial_caps(mes_phrases) + "\n")
