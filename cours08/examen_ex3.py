#!/usr/bin/env python3

# exercice 3

import sys
import requests
from bs4 import BeautifulSoup

interactive = True
article_length_limit = 8

def occurrence(text: str):
    '''
    3.4. Compter l’occurrence de chaque élément. Retourne un dicitonnaire avec chaque mot comme clé, nombre d'occurrences comme valeur.
    '''
    listified = text.lower().split()
    wordfreq = {}
    for word in listified:
        try:
            wordfreq[word] += 1
        except KeyError:
            if word not in wordfreq:
                wordfreq[word] = 1
            else:
                raise
    return wordfreq

def maxlength(wordfreqdict: dict):
    '''
    Fonction pour trouver le mot le plus long dans un dictionnaire avec mots:occurrences en tant que clés:valeurs.
    '''
    wordlengths = []
    for x in wordfreqdict:
        wordlengths.append(len(x))
    return max(wordlengths)

def printdict(worddict: dict, value: str, precision: int = 4, number_results: int = 0):
    '''
    Fonction pour printer des listes jolies de dictionnaires de mots
    '''
    print(" mot" + (maxlength(worddict) - 2) * " " + f"| {value}")
    print((maxlength(worddict) + 2) * "-" + "|" + (len(value) + 2) * "-")
    if number_results <= 0 or number_results > len(worddict):
        number_results = len(worddict)
    for word, count in sorted(worddict.items(), key=lambda x: -x[1])[:number_results]:
        if type(count) is int:
            print(" " + word + (maxlength(worddict) - len(word)) * " " + " | " + f"{count:{len(str(max(worddict.values())))}d}")
        elif type(count) is float:
            print(" " + word + (maxlength(worddict) - len(word)) * " " + " | " + f"{count:.{precision}f}")
        else:
            print(" " + word + (maxlength(worddict) - len(word)) * " " + " | " + f"{count}")

# def printlengths(wordfreqdict: dict):
#     print(" mot" + (maxlength(wordfreqdict) - 2) * " " + "| occurrences")
#     print((maxlength(wordfreqdict) + 2) * "-" + "|-------------")
#     for word, count in sorted(wordfreqdict.items(), key=lambda x: -x[1]):
#         print(" " + word + (maxlength(wordfreqdict) - len(word)) * " " + " | " + str(count))

def liste_traitee(text: str):
    '''
    3.5. Créer une nouvelle liste dans laquelle vous stockez uniquement les mots, supprimer les chiffres et les ponctuations.
    '''
    liste_mots = []
    punctuation = """.,”“?!'":;()-–$£€"""
    numbers = "0123456789"
    for word in text.split():
        for char in word:
            if char in (punctuation + numbers):
                word = word.replace(char, "")
        if len(word) > 0:
            liste_mots.append(word.lower())
    return liste_mots

def occurrence_tr(processed_text: list):
    '''
    3.6. Recalculer l’occurrence de chaque mot à partir de la liste traitée. Retourne un dictionnaire.
    '''
    wordfreq = {}
    for word in processed_text:
        if word not in wordfreq:
            wordfreq[word] = 1
        else:
            wordfreq[word] += 1
    return wordfreq

def frequence_mot(processed_text: list):
    '''
    Calculer la fréquence de chaque mot. Fréquence = nombre d’occurrence d’un mot / nombre d’occurrence de tous les mots * 100.
    '''
    occurrences = occurrence_tr(processed_text)
    freqdict = {}
    # for n in occurrences[n]:
    #     total_occurrences += n
    for word, count in occurrences.items():
        freqdict[word] = count / sum(occurrences.values()) * 100
    return freqdict

# def printfreq(wordfreqdict: dict):
#     print(" mot" + (maxlength(wordfreqdict) - 2) * " " + "| fréquence")
#     print((maxlength(wordfreqdict) + 2) * "-" + "|-------------")
#     for word, count in sorted(wordfreqdict.items(), key=lambda x: -x[1]):
#         print(" " + word + (maxlength(wordfreqdict) - len(word)) * " " + " | " + f"{count:.4f}")

# def printfreq_5(wordfreqdict: dict):
#     '''
#     3.8. Afficher les 5 mots les plus fréquents
#     '''
#     print(" mot" + (maxlength(wordfreqdict) - 2) * " " + "| fréquence")
#     print((maxlength(wordfreqdict) + 2) * "-" + "|-------------")
#     for word, count in sorted(wordfreqdict.items(), key=lambda x: -x[1])[:5]:
#         print(" " + word + (maxlength(wordfreqdict) - len(word)) * " " + " | " + f"{count:.4f}")

# def printdict_n(processed_text: list, nombre: int):
#     '''
#     3.9. Réecrire la fonction précédente qui prend en argument une liste et le nombre de mot les plus fréquents à renvoyer.
#     '''
#     wordfreqdict = frequence_mot(processed_text)
#     print(" mot" + (maxlength(wordfreqdict) - 2) * " " + "| fréquence")
#     print((maxlength(wordfreqdict) + 2) * "-" + "|-------------")
#     for word, count in sorted(wordfreqdict.items(), key=lambda x: -x[1])[:nombre]:
#         print(" " + word + (maxlength(wordfreqdict) - len(word)) * " " + " | " + f"{count:.4f}")
def printdict_n(processed_text: list, value: str, precision: int = 4, number_results: int = 0):
    '''
    3.9. Réecrire la fonction précédente qui prend en argument une liste et le nombre de mot les plus fréquents à renvoyer.
    '''
    worddict = frequence_mot(processed_text)
    printdict(worddict, value, precision, number_results)

def liste_traitee_aumoins4(text: str):
    '''
    3.10. Créer une liste avec les mots qui ont plus de 3 caractères.
    '''
    liste_mots = []
    punctuation = """.,”“?!'":;()-–$£€"""
    numbers = "0123456789"
    for word in text.split():
        if len(word) > 3:
            for char in word:
                if char in (punctuation + numbers):
                    word = word.replace(char, "")
            liste_mots.append(word.lower())
    return liste_mots

def liste_traitee_4a10(text:str):
    """
    3.12. Créer une liste les mots qui ont entre 4 et 10 caractères (inclus).
    """
    liste_mots = []
    punctuation = """.,”“?!'":;()-$£€"""
    numbers = "0123456789"
    for word in text.split():
        if len(word) >= 4 and len(word) <= 10:
            for char in word:
               if char in (punctuation + numbers):
                   word = word.replace(char, "")
            liste_mots.append(word.lower())
    return liste_mots

def lettres_par_mot(processed_text: list):
    """
    3.13. Calculer le nombre de lettres de chaque mot.
    """
    result = {}
    for word in processed_text:
        if word not in result:
            result[word] = len(word)
    return result

def moyenne_lettres_par_mot(length_dict: dict):
    '''
    3.14. Calculer la moyenne de lettres par mot.
    '''
    return sum(length_dict.values()) / len(length_dict)

def mots_longs(processed_text: list):
    '''
    Trouver le(s) mot(s) le(s) plus long(s) dans votre liste initiale.
    '''
    longest_words = [""]
    for word in processed_text:
        if len(word) > len(longest_words[0]):
            longest_words = [word]
        elif len(word) == len(longest_words[0]):
            longest_words.append(word)
    return longest_words

def occurrences_mot(word: str, processed_text: list):
    ocdict = occurrence_tr(processed_text)
    try:
        return ocdict[word]
    except KeyError:
        if word not in ocdict:
            return 0
        else:
            raise

def main():
    print("Exercice 3\n==========\n")
    if len(sys.argv) > 2:
        print("Usage: python examen_ex3.py [address of an article from The Guardian]")
        sys.exit(1)
    if len(sys.argv) == 2:
        req = requests.get(sys.argv[1])
    if len(sys.argv) == 1:
        req = requests.get("https://www.theguardian.com/uk-news/2019/apr/16/more-than-100-people-arrested-in-london-climate-change-protests-extinction-rebellion")
    soup = BeautifulSoup(req.content, "html.parser")
    soup = soup.find('div', class_="content__article-body from-content-api js-article__body")
    soup = soup.find_all('p')
    paras = []
    for para in soup:
        paras.append(para.get_text())
    mon_texte = str()
    global article_length_limit
    if article_length_limit <= 0 or article_length_limit > len(paras):
        article_length_limit = len(paras)
    for text in paras[:article_length_limit]:
        mon_texte += text + " "
    print("3.1. " + mon_texte + "\n")
    print("3.2. " + str(mon_texte.split()) + "\n")
    print(f"3.3. Nombre d'éléments dans la liste : {len(mon_texte.split())} mots.\n")
    print("3.4. Liste de mots dans le texte :\n")
    printdict(occurrence(mon_texte), "occurrences")
    print("\n")
    print(f"3.5. {str(liste_traitee(mon_texte))}\n")
    print("3.6. Liste de mots dans le texte :\n")
    printdict(occurrence_tr(liste_traitee(mon_texte)), "occurrences")
    print("\n")
    print("3.7. Fréquences par mot :\n")
    printdict(frequence_mot(liste_traitee(mon_texte)), "fréquence", 5)
    print("\n")
    print("3.8. Fréquences par mot (top 5) :\n")
    printdict(frequence_mot(liste_traitee(mon_texte)), "fréquence", number_results = 5)
    print("\n")
    print("3.9. Fréquences par mot (top n) :\n")
    if interactive == True:
        n = int(input("Afficher combien de mots ? "))
    else:
        n = 8
    print(f"n = {n}\n")
    printdict_n(liste_traitee(mon_texte), "fréquence", number_results=n)
    print("\n")
    print("3.10. Liste de mots de plus de 3 caractères :\n")
    print(liste_traitee_aumoins4(mon_texte))
    print("\n")
    print("3.11. Fréquence des mots de plus de 3 caractères :\n")
    printdict(frequence_mot(liste_traitee_aumoins4(mon_texte)), "fréquence")
    print("\n")
    print("3.12. Liste de mots de 4 à 10 caractères (inclus) :\n")
    printdict(frequence_mot(liste_traitee_4a10(mon_texte)), "fréquence")
    print("\n")
    print("3.13. Nombre de lettres par mot :\n")
    printdict(lettres_par_mot(liste_traitee(mon_texte)), "nb. lettres")
    print("\n")
    print(f"3.14. Moyenne de lettres par mot: {moyenne_lettres_par_mot(lettres_par_mot(liste_traitee(mon_texte))):.2f}" + "\n")
    print(f"3.15. Mot(s) le(s) plus long(s) du texte : {mots_longs(liste_traitee(mon_texte))}" + "\n")
    if interactive is True:
        recherche = input("3.16. Mot à rechercher : ").lower()
    else:
        recherche = "protest"
    print(f'3.16. Occurrences du mot "{recherche}" : {occurrences_mot(recherche, liste_traitee(mon_texte))}' + '\n')

if __name__ == "__main__":
    main()
