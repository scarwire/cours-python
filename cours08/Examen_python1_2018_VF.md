Examen cours python 1
=====================

Lorsqu'une question comme par f(arg1) merci de créer une fonction avec un argument f(arg1, arg2) -- signifie une fonction avec deux arguments... Appeler la fonction créée et afficher son résultat.

N'oubliez pas de commenter votre code.

Exercice 1 -- Manipulation d'une chaîne de caractère
----------------------------------------------------

1.  Créer une variable nommer « mes\_phrases » contenant deux phrases de votre choix (au minimum 5 mots par phrase et au moins 1 nombre). NB : Une phrase commence par une majuscule et se finit par un signe de ponctuation.

2.  f(arg1) - Supprimer toutes les voyelles de ce texte. Par exemple : « Voici une phrase. » donnera « Vc n phrs. »

3.  f(arg1) - Faire une chaine de caractère sans espace avec tous les mots de votre phrase. Par exemple : « Voiciunephrase. »

4.  f(arg1) - Inverser l'ordre des mots du texte. Par exemple : « Voici une phrase à inverser. » donnera « .inverser à phrase une Voici »

5.  f(arg1) - Mettre une majuscule à chaque première lettre de chaque mot. Par exemple : « Voici une phrase » donnera : « Voici Une Phrase.».

Exercice 2 -- Chiffrer une chaine de caractère
----------------------------------------------

1.  f(arg1) - Créer une variable « mes\_phrases\_sans\_chiffre » qui est une copie « mes\_phrases » mais sans chiffre. Ne pas supprimer les chiffres dans la variable mais le faire à l'aide d'une fonction. Cette fonction doit pouvoir supprimer n'importe quel chiffre de 0 à 9 (nombre entier). Par exemple : « Voici une phrase 1. Voici une phrase 2 » donnera : « Voici une phrase. Voici une phrase».

2.  f(arg1) - Créer une fonction qui prend en argument une phrase qui contient des majuscules et qui retourne une phrase uniquement en minuscule. Stocker le résultat de la fonction dans « mes\_phrases\_minuscules » Par exemple : « Voici une phrase. Voici une phrase » donnera : « voici une phrase. voici une phrase».

3.  f(arg1) - Remplacer chaque lettre de « mes\_phrases\_en\_minuscule » par la lettre qui se situe 2 rang plus loin dans l'alphabet. Stocker le résultat dans « mes\_phrases\_chiffrees » Par exemple : « voici une phrase z» donnera : « xqkek wpg rjtcug b»

4.  f(arg1, arg2) - Réécrire la fonction précédente qui prend en argument une phrase et le rang des lettres à modifier dans l'alphabet. Par exemple ma\_fonction(« abc », 3) renverra « def »

5.  f(arg1, arg2) - Réécrire la fonction en 2.4 qui pourra prendre en argument une phrase avec des majuscules et des minuscules et l'écart des lettres choisis de l'alphabet. Tester la fonction avec « mes\_phrases\_sans\_chiffre ». Exemple : f(« Abc », 2) donne « Cde ».

6.  f(arg1) - Adapter la fonction en 2.5 pour prendre en argument une phrase avec des majuscule, minuscule et des nombres (la variable « mes\_phrases »). Ajoutez 2 à chaque nombre et changer les lettres par les lettres situés deux rangs en arrière dans l'alphabet.

Exercice 3 -- Mots dans un texte
--------------------------------

1.  Récupérer du texte sur un site web. Enregistrer le résultat dans une variable « mon\_texte ».

2.  Parser la variable « mon\_texte » pour obtenir une liste où chaque mot est un élément. Par exemple « Ceci est mon texte récupéré. » donne ma\_liste\[« Ceci », « est », « mon », « texte », « récupéré », « . »\]

3.  Afficher le nombre d'élément dans votre liste.

4.  f(arg1) - Compter l'occurrence de chaque élément.

5.  f(arg1) - Créer une nouvelle liste dans laquelle vous stockez uniquement les mots, supprimer les chiffres et les ponctuations.

6.  f(arg1) - Recalculer l'occurrence de chaque mot.

7.  f(arg1) - Calculer la fréquence de chaque mot. Fréquence = nombre d'occurrence d'un mot / nombre d'occurrence de tous les mots \* 100.

8.  f(arg1) - Afficher les 5 mots les plus fréquents.

9.  f(arg1, arg2) - Réecrire la fonction précédente qui prend en argument une liste et le nombre de mot les plus fréquents à renvoyer.

10. f(arg1) -- Créer une liste avec les mots qui ont plus de 3 caractères

11. Recalculer la fréquence des mots de plus de 3 caractères.

12. f(arg1) -- Créer une liste les mots qui ont entre 4 et 10 caractères (inclus).

13. f(arg1) - Calculer le nombre de lettre de chaque mot.

14. f(arg1) - Calculer la moyenne de lettre par mot.

15. f(arg1) - Trouver le(s) mot le plus long dans votre liste initiale.

16. f(arg1, arg2) - Créer une fonction pour rechercher un mot et indiquer le nombre de fois qu'il apparaît dans le texte.

Exercice 4 -- Phrases dans un texte
-----------------------------------

1.  Récupérer du texte sur un site web. Enregistrer le résultat dans une variable « mon\_texte\_phrase ».

2.  Parser la variable « mon\_texte\_phrase » pour obtenir une liste où chaque phrase est un élément. Par exemple « Phrase 1. Phrase 2.» donne ma\_liste \[« Phrase 1. », « Phrase 2. »\]

3.  f(arg1) - Calculer le nombre de phrases.

4.  f(arg1) - Calculer le nombre de mots par phrases. A noter qu'un nombre écrit en chiffre ou une ponctuation n'est pas un mot.

5.  f(arg1) - Calculer la moyenne de mots par phrases.

6.  f(arg1) - Trouver la phrase avec le plus de mot.

7.  f(arg1) - Trouver la phrase avec le plus de caractères.

8.  f(arg1, arg2) - Trouver les phrases qui contiennent mot recherché au moment de l'appel à la fonction. (f(texte, mot\_a\_trouver))

Exercice 5 -- Comparaison de texte
----------------------------------

1.  Créez deux fichiers .txt contenant une vingtaine de lignes (30 ou 10 ça marche aussi)

2.  f(arg1, arg2) - Créer une fonction pour déterminer quel texte contient le plus de mot.

3.  f(arg1, arg2) - Créer une fonction pour déterminer quel texte contient le plus de phrase.

4.  f(arg1, arg2, arg3) - Créer une fonction pour savoir quel texte contient a le plus de fois un mot de votre choix (qui devra être un argument de votre fonction).

Exercice 6 -- Lecture et écriture dans un tableau
-------------------------------------------------

1.  f(arg1) - Ecrire une fonction qui dans un tableur inscrit dans une colonne le nombre de mot et dans l'autre le nombre d'occurrence de ce mot d'un texte de votre choix.

2.  f(arg1) - Créer une troisième colonne avec la fréquence de chaque mot.

3.  f(arg1, arg2) - Afficher uniquement les mots qui ont une fréquence supérieure à 5% (Si vous n'avez pas de mot avec une présence \>= à 5% choisissez une autre seuil).

4.  f(arg1) - Calculer la moyenne de fréquence des mots de votre texte.

5.  f(arg1) - Trouver la fréquence minimale et maximale.

6.  En fonction des résultats précédents créer 3 catégories : frequence\_faible, frequence\_moyenne et frequence\_elevee. Par exemple si ma fréquence moyenne est de 7%, fréquence minimale de 1% et ma fréquence maximale de 15%, frequence\_faible : \< 5%, frequence\_moyenne : de 5% à 8% et frequence\_elevee : \>8%

7.  f(arg1) - Créer une quatrième colonne où apparaît le niveau de fréquence de chaque mot : faible, moyenne, elevee.
