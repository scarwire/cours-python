#!/usr/bin/env python

# exercice 4

import sys
import requests
from bs4 import BeautifulSoup

interactive = True
article_length_limit = 8

if len(sys.argv) > 2:
    print("Usage: python examen_ex4.py [address of an article from The Guardian]")
    sys.exit(1)
if len(sys.argv) == 2:
    req = requests.get(sys.argv[1])
if len(sys.argv) == 1:
    req = requests.get("https://www.theguardian.com/uk-news/2019/apr/16/more-than-100-people-arrested-in-london-climate-change-protests-extinction-rebellion")

print("Exercice 4\n==========\n")

soup = BeautifulSoup(req.content, "html.parser")
soup = soup.find('div', class_="content__article-body from-content-api js-article__body")
soup = soup.find_all('p')
paras = []
for para in soup:
    paras.append(para.get_text())
mon_texte_phrase = ""
if article_length_limit <= 0 or article_length_limit > len(paras):
    article_length_limit = len(paras)
for text in paras[:article_length_limit]:
    mon_texte_phrase += text + " "
mon_texte_phrase = mon_texte_phrase.replace("\xa0", " ")
mon_texte_phrase = mon_texte_phrase.replace("  ", " ")
print("4.1. " + mon_texte_phrase + "\n")

def pretraitement(text: str):
    '''
    Enlever ponctuation et chiffres, séparer texte en liste de phrases.
    '''
    liste_phrases = []
    punctuation = """,”“?!'":;()-–$£€"""
    # numbers = "0123456789"
    for char in text:
        if char in punctuation: #(punctuation + numbers):
            text = text.replace(char, "")
    text = text.replace("  ", " ")
    sentences = text.split(". ")
    for sentence in sentences:
        if len(sentence) <= 1:
            sentences.remove(sentence)
    return sentences

liste_phrases = pretraitement(mon_texte_phrase)
print("4.2. " + str(liste_phrases) + "\n")

def compter_phrases(processed_text: list):
    '''
    4.3. Calculer le nombre de phrases.
    '''
    return len(processed_text)

print(f"4.3. Nombre de phrases dans l'extrait : {compter_phrases(liste_phrases)}" + "\n")

def mots_par_phrase(sentence: str):
    """
    4.4. Calculer le nombre de mots par phrases. A noter qu’un nombre écrit en chiffre ou une ponctuation n’est pas un mot.
    """
    words = sentence.split()
    for word in words:
        for char in word:
            if char in "0123456789":
                word = word.replace(char, "")
    #for word in words:
    #    if word in ["", " ", "  "]:
    #        words.remove(word)
    return len(words)

longueurs = []
for sentence in liste_phrases:
    longueurs.append(mots_par_phrase(sentence))

print("4.4. Mots par phrase :\n")
print(" phrase |  mots")
print("--------|-------")
i = 0
for element in longueurs:
    print(f" {i + 1:6d} | {element:5d} ")
    i += 1
print("\n")

def moyenne_mots_par_phrase(processed_text: list):
    """
    4.5. Calculer la moyenne de mots par phrases.
    """
    lengths = []
    for sentence in processed_text:
        lengths.append(mots_par_phrase(sentence))
    return sum(lengths) / len(lengths)

print(f"4.5. Moyenne de mots par phrase : {moyenne_mots_par_phrase(liste_phrases):.1f}" + "\n")

def maxiphrase(processed_text: list):
    '''
    4.6. Trouver la phrase avec le plus de caractères.
    '''
    longest_sentence_length = 0
    longest_setence_indexes = []
    i = 0
    while i < len(processed_text):
        if len(processed_text[i]) > longest_sentence_length:
            longest_sentence_length = len(processed_text[i])
            longest_setence_indexes = [i]
        elif len(processed_text[i]) == longest_sentence_length:
            longest_setence_indexes.append(i)
        i += 1
    return (longest_sentence_length, longest_setence_indexes)

maxlength, maxsentences = maxiphrase(liste_phrases)
if len(maxsentences) == 1:
    print(f'3.7. La phrase n° {maxsentences[0] + 1} est celle qui contient le plus de caractères ({maxlength}) : "{liste_phrases[maxsentences[0]]}."\n')
if len(maxsentences) > 1:
    print(f"3.7. Les phrases suivantes contiennent le plus de caractères ({maxlength}) :")
    for element in maxsentences:
        print(f'Phrase n° {element + 1} : "{liste_phrases[element]}"')
    print("\n")

def recherche_mot_phrase(processed_text: list, search: str):
    '''
    4.7 Trouver les phrases qui contiennent mot recherché au moment de l'appel à la fonction. (f(texte, mot\_a\_trouver))
    '''
    matching_sentences = []
    i = 0
    for sentence in processed_text:
        for word in sentence.split():
            if i in matching_sentences:
                pass
            elif search.lower() == word.lower():
                matching_sentences.append(i)
        i += 1
    return matching_sentences

if interactive is True:
    recherche = input("4.7. Entrez un mot à rechercher dans le texte : ")
else:
    recherche = "climate"
    print(f'4.7. Mot à rechercher : "{recherche}".')

if len(recherche_mot_phrase(liste_phrases, recherche)) == 0:
    print(f'''Le mot "{recherche}" n'est pas présent dans le texte.''')
else:
    print(f'Phrase(s) où se trouve(nt) le mot "{recherche}" :')
    for sentence in recherche_mot_phrase(liste_phrases, recherche):
        print(f'Phrase {sentence + 1}: "{liste_phrases[sentence]}."')
