import folium
from random import randint

locations = []
for i in range(15):
    marker_variable = folium.Marker([19.4027 + randint(-10, 10) / 100., -99.1609 + randint(-10, 10) / 100.])
    locations.append(marker_variable)

cdmx = folium.Map(location = [19.4027, -99.1609], tiles = 'Stamen Terrain', zoom_start = 11)

# ajouter les marqueurs
for e in locations:
    e.add_to(cdmx)

cdmx.save("results/cdmx2.html")